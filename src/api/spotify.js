import axios from 'axios';
import { store } from '../store/index.js';

const base_url = 'https://api.spotify.com/v1';

export async function getToken(client_id, client_secret) {
  const response = await axios.post(
    "https://accounts.spotify.com/api/token",
    "grant_type=client_credentials",
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Basic " + window.btoa(client_id + ':' + client_secret),
      },
    }
  );

  store.commit('setAccessToken', response.data.access_token);

  return response.data;
}

export function getHeaders() {
  const access_token = store.state.access_token;

  return { Authorization: "Bearer " + access_token }
}

export async function getArtist(id) {
  const response = await axios.get(
    base_url + `/artists/` + id,
    {
      headers: getHeaders(),
    }
  );
  const data = response.data;
  return data;
}

export async function searchArtist(id) {
  const response = await axios.get(
    base_url + `/search?q=${id}&type=artist&limit=1&market=FR`,
    {
      headers: getHeaders(),
    }
  );
  const data = response.data;
  return data.artists.items;
}

export async function getAlbums(idArtist) {
  const response = await axios.get(
    base_url + `/artists/${idArtist}/albums?include_groups=album&market=FR`,
    {
      headers: getHeaders(),
    }
  );
  return response.data;
}

export async function getAlbumTracks(id) {
  const albumTracksResponse = await axios.get(
    base_url + `/albums/${id}/tracks`,
    {
      headers: getHeaders(),
    }
  );
  
  const albumTracksData = albumTracksResponse.data;
  
  const tracksInfoResponse = await axios.get(
    base_url + `/tracks?ids=${albumTracksData.items.map(t => t.id).join(',')}`,
    {
      headers: getHeaders(),
    }
  );
  const tracksInfoData = tracksInfoResponse.data;
  
  return tracksInfoData.tracks.sort(function(a, b) {
    return b.popularity - a.popularity;
  });
}