import { createRouter, createWebHistory } from 'vue-router';
import Home from "../views/Home.vue";
import Artist from "../views/Artist.vue";

export const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', name: 'home', component: Home },
        { path: '/artist/:id', component: Artist },
    ]
});