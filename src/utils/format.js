export const formatDurationMsToDurationMins = (durationMs) => {
    if (!Number.isInteger(durationMs)) return '';
    const minutes = Math.floor(durationMs / 60000);
    const seconds = Math.floor((durationMs % 60000) / 1000);
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
};

export const getYear = (date) => {
    return date.split('-')[0];
}