import { createStore } from 'vuex';
import { getToken } from '../api/spotify.js';

export const store = createStore({
  state: {
    access_token: localStorage.access_token,
  },
  getters: {
  },
  mutations: {
    setAccessToken(state, token) {
      state.access_token = token;
      localStorage.access_token = token;
    },
  },
  actions: {
    async getToken({ commit }) {
      const client_id = process.env.VUE_APP_SPOTIFY_CLIENT_ID;
      const client_secret = process.env.VUE_APP_SPOTIFY_CLIENT_SECRET;
      const data = await getToken(client_id, client_secret);
      commit('setAccessToken', data.access_token);
    },
  },
  modules: {
  }
});