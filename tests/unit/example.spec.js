import { shallowMount } from '@vue/test-utils'
import Header from '@/components/Header.vue'

describe('Header.vue', () => {
  it('renders a div', () => {
    const wrapper = shallowMount(Header)
    expect(wrapper.contains('div')).toBe(true)
  })
})
