# Antidots Spotify

Création d'une application Vuejs permettant d'afficher les albums d'un artiste Spotify en fonction de sa popularité.
Pour l'affichage des albums, je me suis basé sur la page https://open.spotify.com/artist/0eDvMgVFoNV3TpwtrVCoTj/discography/album

## Prérequis
- Nodejs
- NPM
- Docker (optionnel)

## Installation
```sh
    git clone git@gitlab.com:rodseb/antidots-spotify.git
    
    cd antidots-spotify
    
    npm install

    npm run serve
```

## Tests
```sh
    npm run test:unit
```

## Librairies
- Vuex
- Vue router
- Tailwind
- JEST

## Environment variables
- VUE_APP_SPOTIFY_CLIENT_ID
- VUE_APP_SPOTIFY_CLIENT_SECRET
(https://developer.spotify.com/documentation/web-api/tutorials/getting-started)

## Installation docker
```sh
    docker build -t vuejs .
    
    docker run -d --rm -p 8000:8000 --name vuejs vuejs
```
